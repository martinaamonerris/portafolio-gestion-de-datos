import mysql.connector

def crear_tabla_usuarios():
    # Conectarse a la base de datos (asegúrate de tener el servidor MySQL en ejecución)
    conexion = mysql.connector.connect(
        host="localhost",
        user="root",
        password="root",
        database="prueba"
    )

    # Crear un cursor para ejecutar comandos SQL
    cursor = conexion.cursor()

    # Crear la tabla de usuarios si no existe
    cur.execute('''CREATE TABLE IF NOT EXISTS User (
                            id INT AUTO_INCREMENT PRIMARY KEY,
                            title CHAR(64),
                            name CHAR(32),
                            surname CHAR(32),
                            email CHAR(64)
                        )''')

    # Guardar los cambios y cerrar la conexión
    conexion.commit()
    conexion.close()

# Llamar a la función para crear la tabla de usuarios
crear_tabla_usuarios()
